package com.csu.mypetstoreapi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Objects;

@Data
@TableName("category")
public class Category {

    @TableId(value = "catid",type = IdType.INPUT)
    private String categoryId;

    private String name;

    @TableField(value = "descn")
    private String description;

}
package com.csu.mypetstoreapi.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@TableName("inventory")
public class Inventory {

    private String itemid;
    private int qty;
}

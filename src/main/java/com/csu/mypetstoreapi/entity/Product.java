package com.csu.mypetstoreapi.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("product")
public class Product implements Serializable {

  private static final long serialVersionUID = -7492639752670189553L;
  @TableId("productid")
  private String productId;
  @TableField("category")
  private String categoryId;
  private String name;
  @TableField("descn")
  private String description;

}

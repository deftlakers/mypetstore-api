package com.csu.mypetstoreapi.entity;

import lombok.Data;

@Data
public class Account {

    String username;
    String password;
}

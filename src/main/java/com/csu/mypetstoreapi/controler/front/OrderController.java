package com.csu.mypetstoreapi.controler.front;

import com.csu.mypetstoreapi.service.OrderService;
import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.entity.Order;
import com.csu.mypetstoreapi.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/Order/")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("orders")
    @ResponseBody
    public CommonResponse<List<OrderVO>> getOrderList() {
        return orderService.getOrderList();
    }

    @GetMapping("getOrderById")
    @ResponseBody
    public CommonResponse<OrderVO> getOrderById(Integer id) {
        return orderService.getOrderById(id);
    }
}

package com.csu.mypetstoreapi.controler.front;


import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.entity.Account;
import com.csu.mypetstoreapi.entity.CartItem;
import com.csu.mypetstoreapi.entity.Item;
import com.csu.mypetstoreapi.service.CartService;
import com.csu.mypetstoreapi.service.CatalogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/cart")
public class CartController {


    @Autowired
    private CartService cartService;
    @Autowired
    private CatalogService catalogService;

    HttpSession session;
    Account account;


    @GetMapping("/cartItems")
    @ResponseBody
    public CommonResponse<List<CartItem>> getCart(Account account){
        return cartService.getCart(account);
    }

    @PostMapping("/cartItems")
    @ResponseBody
    public CommonResponse<List<CartItem>> addCart(HttpServletRequest request, Model model){
        session = request.getSession();
        account = (Account)session.getAttribute("account") ;
        String workingItemId = request.getParameter("workingItemId");

        if(account == null){
//            APP.log(session,request,"添加物品失败");
            session.setAttribute("message","请先登录");
            return CommonResponse.createForError("请先登录");
        }
        else{
            CartItem cartItem = new CartItem();

            Item item = catalogService.getItemById(workingItemId);

            cartItem.setUserid(account.getUsername());
            cartItem.setItem(item);
            cartItem.setQuantity(1);
            cartItem.setInStock(catalogService.isItemInStock(workingItemId));


            cartService.addItem(cartItem);

            List<CartItem> cart = cartService.getCart(account).getData();
            BigDecimal subTotal = cartService.getTotal(account);
            model.addAttribute("cartList",cart);
            session.setAttribute("cartList",cart);
            session.setAttribute("totalCost",subTotal);
            //APP.log(session,request,"添加物品 "+cartItem.getItem().getItemId()+" 到购物车");
            cart.clear();
            cart.add(cartItem);
            //返回增加得信息
            return CommonResponse.createForSuccess(cart);
        }
    }

    @DeleteMapping("/cartItems")
    @ResponseBody
    public Object removeItem(HttpServletRequest request){

        HttpSession session = request.getSession();
        String workingItemId = request.getParameter("itemId");

        Account account = (Account)session.getAttribute("account");

        CartItem item = cartService.getItemById(workingItemId,account.getUsername());

        cartService.removeCartItem(item);
        BigDecimal subTotal = cartService.getTotal(account);


        List<CartItem> list = new ArrayList<>();
        list.add(item);

        return CommonResponse.createForSuccess(list);
    }

    @ResponseBody
    @PutMapping("/cartItems")
    public Object cartChange(HttpServletRequest request){
        HttpSession session = request.getSession();

        Account account = (Account)session.getAttribute("account");

        String itemId = request.getParameter("itemid");
        int newQuantity = Integer.parseInt(request.getParameter("quantity"));
        cartService.updateQuantity(itemId,newQuantity,account.getUsername());

        List<CartItem> cart = cartService.getCart(account).getData();
        BigDecimal subTotal = cartService.getTotal(account);

        CartItem item = cartService.getItemById(itemId,account.getUsername());

        List<CartItem> list = new ArrayList<>();
        list.add(item);

        return CommonResponse.createForSuccess(list);
    }


}

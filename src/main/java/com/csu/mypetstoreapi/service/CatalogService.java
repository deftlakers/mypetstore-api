package com.csu.mypetstoreapi.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.entity.Category;
import com.csu.mypetstoreapi.entity.Inventory;
import com.csu.mypetstoreapi.entity.Item;
import com.csu.mypetstoreapi.entity.Product;
import com.csu.mypetstoreapi.persistence.CategoryMapper;
import com.csu.mypetstoreapi.persistence.InventoryMapper;
import com.csu.mypetstoreapi.persistence.ItemMapper;
import com.csu.mypetstoreapi.persistence.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private InventoryMapper inventoryMapper;

    public CommonResponse<List<Category>> getCategoryList(){
        List<Category> list = categoryMapper.selectList(null);
        if(list.size() != 0 ){
            return CommonResponse.createForSuccess(list);
        }
        else return CommonResponse.createForSuccessMessage("没有分类信息");
    }


    public Item getItemById(String itemId){
        //获取item表中的值
        QueryWrapper<Item> itemQueryWrapper = new QueryWrapper<>();
        itemQueryWrapper.eq("itemid",itemId);
        Item item = itemMapper.selectOne(itemQueryWrapper);

        //获取product表中的值，注入item
        QueryWrapper<Product>productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq("productid",item.getProductId());
        item.setProduct(productMapper.selectOne(productQueryWrapper));

        //从inventory表中获取quantity，注入item
        QueryWrapper<Inventory>inventoryQueryWrapper = new QueryWrapper<>();
        inventoryQueryWrapper.eq("itemid",itemId);
        item.setQuantity(inventoryMapper.selectOne(inventoryQueryWrapper).getQty());

        return item;
    }

    public boolean isItemInStock(String itemId){
        QueryWrapper<Inventory> itemQueryWrapper = new QueryWrapper<>();
        itemQueryWrapper.eq("itemid",itemId);
        Inventory inventory =  inventoryMapper.selectOne(itemQueryWrapper);
        return inventory.getQty()>0 ;
    }
}

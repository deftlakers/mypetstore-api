package com.csu.mypetstoreapi.service.impl;

import com.csu.mypetstoreapi.entity.OrderStatus;
import com.csu.mypetstoreapi.persistence.OrderStatusMapper;
import com.csu.mypetstoreapi.service.OrderService;
import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.common.ResponseCode;
import com.csu.mypetstoreapi.entity.Order;
import com.csu.mypetstoreapi.persistence.OrderMapper;
import com.csu.mypetstoreapi.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private OrderStatusMapper orderStatusMapper;

    @Override
    public CommonResponse<List<OrderVO>> getOrderList() {
        List<Order> orderList = orderMapper.selectList(null);
        if (orderList.size() != 0) {
            return CommonResponse.createForSuccess(entityListToOrderVO(orderList));
        }
        return CommonResponse.createForSuccessMessage("没有订单");
    }

    @Override
    public CommonResponse<OrderVO> getOrderById(Integer orderId) {
        if (orderId == null) {
            return CommonResponse.createForError(
                    ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDescription());
        }
        Order order = orderMapper.selectById(orderId);

        if (order == null) {
            return CommonResponse.createForError("该订单不存在或已删除");
        }
        return CommonResponse.createForSuccess(entityToOrderVO(order));
    }

    OrderVO entityToOrderVO(Order order) {
        OrderVO orderVO = new OrderVO();

        orderVO.setOrderId(order.getOrderId());
        orderVO.setUsername(order.getUsername());
        orderVO.setOrderDate(order.getOrderDate());
        orderVO.setShipAddress1(order.getShipAddress1());
        orderVO.setShipAddress2(order.getShipAddress2());
        orderVO.setShipCity(order.getShipCity());
        orderVO.setShipState(order.getShipState());
        orderVO.setShipZip(order.getShipZip());
        orderVO.setShipCountry(order.getShipCountry());
        orderVO.setBillAddress1(order.getBillAddress1());
        orderVO.setBillAddress2(order.getBillAddress2());
        orderVO.setBillCity(order.getBillCity());
        orderVO.setBillCountry(order.getBillCountry());
        orderVO.setBillZip(order.getBillZip());
        orderVO.setBillCity(order.getBillCity());
        orderVO.setCourier(order.getCourier());
        orderVO.setTotalPrice(order.getTotalPrice());
        orderVO.setBillToFirstName(order.getBillToFirstName());
        orderVO.setBillToLastName(order.getBillToLastName());
        orderVO.setShipToFirstName(order.getShipToFirstName());
        orderVO.setShipToLastName(order.getShipToLastName());
        orderVO.setCreditCard(order.getCreditCard());
        orderVO.setExpiryDate(order.getExpiryDate());
        orderVO.setCardType(order.getCardType());
        orderVO.setLocale(order.getLocale());

        OrderStatus orderStatus = orderStatusMapper.selectById(order.getOrderId());
        orderVO.setStatus(orderStatus.getStatus());
        return orderVO;
    }

    List<OrderVO> entityListToOrderVO(List<Order> list) {
        List<OrderVO> orderVOList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            orderVOList.add(entityToOrderVO(list.get(i)));
        }
        return orderVOList;
    }
}

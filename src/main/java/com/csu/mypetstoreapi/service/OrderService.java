package com.csu.mypetstoreapi.service;

import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.entity.Order;
import com.csu.mypetstoreapi.vo.OrderVO;

import java.util.List;

public interface OrderService {
    CommonResponse<List<OrderVO>> getOrderList();
    CommonResponse<OrderVO> getOrderById(Integer orderId);
}

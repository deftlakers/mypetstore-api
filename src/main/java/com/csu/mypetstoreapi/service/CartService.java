package com.csu.mypetstoreapi.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.csu.mypetstoreapi.common.CommonResponse;
import com.csu.mypetstoreapi.entity.Account;
import com.csu.mypetstoreapi.entity.CartItem;
import com.csu.mypetstoreapi.entity.Inventory;
import com.csu.mypetstoreapi.entity.Item;
import com.csu.mypetstoreapi.persistence.CartItemMapper;
import com.csu.mypetstoreapi.persistence.InventoryMapper;
import com.csu.mypetstoreapi.persistence.ItemMapper;
import com.csu.mypetstoreapi.persistence.ProductMapper;
import com.csu.mypetstoreapi.vo.CartItemVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CartService {

    @Autowired
    private CartItemMapper cartItemMapper;
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private InventoryMapper inventoryMapper;
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private CatalogService catalogService;


    public CommonResponse<List<CartItem>> getCart(Account account) {


        QueryWrapper<CartItem>cartItemQueryWrapper = new QueryWrapper<>();
        cartItemQueryWrapper.eq("userid",account.getUsername());
        List<CartItem> list = cartItemMapper.selectList(cartItemQueryWrapper);

        Iterator<CartItem> iterator = list.listIterator();
        while(iterator.hasNext()){
            CartItem cartItem = iterator.next();
            cartItem.setItem(catalogService.getItemById(cartItem.getItemid()));
        }

        return CommonResponse.createForSuccess(list);
    }

    public void addItem(CartItem cartItem){
        cartItemMapper.insert(cartItem);
    }

    public CartItem getItemById(String itemid,String userid){

        QueryWrapper<CartItem>cartItemQueryWrapper = new QueryWrapper<>();
        cartItemQueryWrapper.eq("userid",userid);
        cartItemQueryWrapper.eq("itemid",itemid);
        CartItem cartItem =  cartItemMapper.selectOne(cartItemQueryWrapper);

        cartItem.setItem(catalogService.getItemById(cartItem.getItemid()));

        return cartItem;

    }


    public void removeCartItem(CartItem item){
        cartItemMapper.deleteById(item.getItemid());
    }


    public void clearCart(Account account){

        Iterator<CartItem> iterator = getCart(account).getData().iterator();
        while (iterator.hasNext()){
            removeCartItem(iterator.next());
        }
    }

    public void updateQuantity(String id,int quantity,String userid){


        CartItem cartItem = getItemById(id, userid);
        cartItem.setQuantity(quantity);

        //更新数据库中商品数量
        UpdateWrapper<CartItem> cartItemUpdateWrapper = new UpdateWrapper<>();
        cartItemUpdateWrapper.eq("itemId",id).eq("username",userid);
        cartItemMapper.update(cartItem,cartItemUpdateWrapper);
    }
    public BigDecimal getTotal(Account account){
        BigDecimal subTotal = new BigDecimal("0");
        List<CartItem> list = getCart(account).getData();
        for(int i = 0;i < list.size();i++){
            CartItem cartItem = list.get(i);
            Item item = cartItem.getItem();
            BigDecimal listPrice = item.getListPrice();
            BigDecimal quantity = new BigDecimal(String.valueOf(cartItem.getQuantity()));
            subTotal = subTotal.add(listPrice.multiply(quantity));
        }
        return subTotal;
    }


}

package com.csu.mypetstoreapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.csu.mypetstoreapi.persistence")
public class MyPetStoreApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyPetStoreApiApplication.class, args);
    }

}

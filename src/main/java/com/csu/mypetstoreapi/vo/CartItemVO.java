package com.csu.mypetstoreapi.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.csu.mypetstoreapi.entity.Item;
import com.csu.mypetstoreapi.entity.Product;

import java.math.BigDecimal;

public class CartItemVO {

    private String userid;
    private String itemid;
    private String productid;
    private int quantity;
    private boolean inStock;
    private BigDecimal total;


    private String itemId;
    private String productId;
    private String itemName;
    private BigDecimal listPrice;
    private BigDecimal unitCost;
    private int supplierId;
    private String status;
    private String attribute1;
    private String attribute2;
    private String attribute3;
    private String attribute4;
    private String attribute5;

    private String categoryId;
    private String name;
    private String description;

}

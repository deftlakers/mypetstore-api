package com.csu.mypetstoreapi.vo;

import java.math.BigDecimal;

public class ItemVO {

    private String itemId;
    private String productId;
    private String itemName;
    private BigDecimal listPrice;
    private BigDecimal unitCost;
    private int supplierId;
    private String status;
    private String attribute1;
    private String attribute2;
    private String attribute3;
    private String attribute4;
    private String attribute5;




    private String categoryId;
    private String name;
    private String description;

    private int quantity;



}
